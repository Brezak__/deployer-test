use std::error::Error;
use std::fs::File;
use gix::discover;
use std::io::Write;
use std::time::SystemTime;
use humantime::format_rfc3339;

fn main() -> Result<(), Box<dyn Error>> {
    let base_path = "..";

    let repo = discover(base_path)?;
    println!("Found repository at {}.", repo.path().display());

    let head = repo.head()?;
    let id = head.id().ok_or("Failed to get head Id!")?;

    for input in ["../input-1/text.txt", "../input-2/text.txt", "../input-3/text.txt", "../input-4/text.txt"] {
        let mut file = File::create(input)?;

        let now = SystemTime::now();
        write!(file, "This file was written to on {}.\nThe HEAD commit id was {id}.\nThe path used was {input}", format_rfc3339(now))?;
    }
    
    Ok(())
}
