<?php
namespace Deployer;

require 'recipe/common.php';

// Config

set('repository', 'https://gitlab.com/Brezak__/deployer-test.git');

add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);
set('keep_releases', 4);

// Hosts
localhost()
    ->set('deploy_path', '/home/brezak/Projects/N/deployer-even-more-test/deploy_2');

$hosts = [
    'server1' => '172.17.0.2',
    'server2' => '172.17.0.3',
    'server3' => '172.17.0.4',
];

foreach ($hosts as $host => $hostname) {
    host($hostname)
        //.set('hostname', $hostname)
        .set('remote_user', 'root')
        .set('deploy_path', '~/here');
}

// Hooks

after('deploy:failed', 'deploy:unlock');
