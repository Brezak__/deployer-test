FROM ubuntu:latest

# Install SSH server
RUN apt-get update && apt-get install -y openssh-server git

# Configure SSH server
RUN mkdir /var/run/sshd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin without-password/' /etc/ssh/sshd_config

# Create SSH directory
RUN mkdir /root/.ssh

# Add authorized keys
COPY key.pub /root/.ssh/authorized_keys

# Set correct permissions
RUN chmod 700 /root/.ssh
RUN chmod 600 /root/.ssh/authorized_keys

# Expose SSH port
EXPOSE 22

# Start SSH server
CMD ["/usr/sbin/sshd", "-D"]